#! /usr/bin/env python

from math import atan2, acos, cos, radians, sin, sqrt
from typing import Any, Dict, List

from dateutil.parser import parse
from lxml import etree
import more_itertools as mi


class Point:
    lat: float
    lon: float
    ele: float
    time: str
    timestamp: float
    temp: float
    hr: int
    cad: int

    def __init__(self, tree: etree._Element) -> None:

        self.tree = tree
        _extensions = {
            ele.tag: ele.text
            for ele in self.tree.find("extensions/TrackPointExtension")
        }
        _time = parse(self.tree.find("time").text)

        self.attrs = {
            "lat": float(self.tree.get("lat")),
            "lon": float(self.tree.get("lon")),
            "elev": float(self.tree.find("ele").text),
            "time": _time.timestamp(),
        }

        for k, v in _extensions.items():
            if k == "atemp":
                self.attrs["temp"] = float(v)
            else:
                self.attrs[k] = int(v)

        for k, v in self.attrs.items():
            setattr(self, k, v)

    def distance(self, other: "Point") -> float:
        """Calculate distance between coordinates using equirectangular approximation."""
        x = (radians(other.lon) - radians(self.lon)) * cos((radians(self.lat) + radians(other.lat)) / 2)
        y = radians(other.lat) - radians(self.lat)

        return sqrt(x ** 2 + y ** 2) * 6371

    def delta_t(self, other: "Point") -> float:
        return self.time - other.time

    def to_json(self) -> Dict[str, Any]:
        return self.attrs

    def __eq__(self, other):
        return self.tree == other.tree

    def __repr__(self):
        return f"{self.__class__.__name__}({self.tree})"


class Segment:
    points: List[Point]

    def __init__(self, tree: etree._Element) -> None:
        self.tree = tree

    @property
    def points(self) -> List[Point]:
        if not hasattr(self, "_points"):
            self._points = [Point(trkpt) for trkpt in self.tree.iter(tag="trkpt")]
        return self._points

    def to_json(self) -> List[Dict[str, Any]]:
        return [point.to_json() for point in self.points]

    def __repr__(self):
        return f"{self.__class__.__name__}({self.tree})"


class Track:
    name: str
    type_: str
    segments: List[Segment]

    def __init__(self, tree: etree._Element) -> None:
        self.tree = tree
        self.name = self.tree.find("name").text
        self.type_ = self.tree.find("type").text

    @property
    def segments(self) -> List[Segment]:
        if not hasattr(self, "_segments"):
            self._segments = [
                Segment(trkseg) for trkseg in self.tree.iter(tag="trkseg")
            ]
        return self._segments

    def to_json(self) -> Dict[str, Any]:
        return {
            "name": self.name,
            "type": self.type_,
            "n_segments": len(self.segments),
            "segments": [seg.to_json() for seg in self.segments],
        }

    def __repr__(self):
        return f"{self.__class__.__name__}({self.tree})"


class GPX:

    time: str
    tracks: List[Track]
    segments: List[Segment]
    points: List[Point]

    def __init__(self, src: str, **kwargs):
        self.tree = self._strip_ns(
            etree.parse(src, etree.XMLParser(recover=True, **kwargs))
        )
        self.time = self.tree.find("metadata/time").text

    def _strip_ns(self, elt: etree._ElementTree) -> etree._Element:
        """Strip namespace annotations from tags."""
        ele = etree.fromstring(etree.tostring(elt))
        for element in ele.getiterator():
            element.tag = etree.QName(element).localname
        return ele

    @property
    def tracks(self) -> List[Track]:
        if not hasattr(self, "_tracks"):
            self._tracks = [Track(trk) for trk in self.tree.iter(tag="trk")]
        return self._tracks

    @property
    def segments(self) -> List[Segment]:
        if not hasattr(self, "_segments"):
            self._segments = [
                Segment(trkseg) for trkseg in self.tree.iter(tag="trkseg")
            ]
        return self._segments

    @property
    def points(self) -> List[Point]:
        if not hasattr(self, "_points"):
            self._points = [Point(trkpt) for trkpt in self.tree.iter(tag="trkpt")]
        return self._points

    def to_json(self) -> Dict[str, Any]:
        return {
            "time": self.time,
            "n_tracks": len(self.tracks),
            "tracks": [trk.to_json() for trk in self.tracks],
        }


if __name__ == "__main__":
    gpx = GPX("activity_7210053760.gpx")

    pairs = mi.windowed(gpx.points, 2)
    for p in pairs:
        a, b = p
        print(f"d = {b.distance(a) * 1000}, dt = {b.delta_t(a)}")


